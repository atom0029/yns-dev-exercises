<!DOCTYPE html>
<html>

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<style>
		body {
			margin: 0;
			font-family: Arial, Helvetica, sans-serif;
		}

		.topnav {
			overflow: hidden;
			background-color: #333;
		}

		.topnav a {
			float: left;
			color: #f2f2f2;
			text-align: center;
			padding: 14px 16px;
			text-decoration: none;
			font-size: 17px;
		}

		.topnav a:hover {
			background-color: #ddd;
			color: black;
		}

		.topnav a.active {
			background-color: #04AA6D;
			color: white;
		}
	</style>
</head>

<body>

	<div class="topnav">
		<a style="color: white; cursor: pointer;" class="active html_php">HTML and PHP</a>
		<a style="color: white; cursor: pointer;" class="javscript_exe">JavaScript</a>
		<a style="color: white; cursor: pointer;" class="database">Database</a>
		<a style="color: white; cursor: pointer;" class="sql">Advance Sql</a>
		<a style="color: white; cursor: pointer;" class="practice">Practice Systems / Programs</a>

	</div>



	<div style="padding:50px;" class="div_html_php">
		<h1> HTML and PHP </h1>
		<table class="table">
			<thead>
				<tr>
					<th width="70px">No.</th>
					<th>Title</th>
					<th>Description</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1-1</td>
					<td>Show Hello World</td>
					<td>Show "Hello World" in a page.</td>
					<td><a href="../Html Php/1-1.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>1-2</td>
					<td>The four basic operations of arithmetic.</td>
					<td style="justify-content: center;">Display 2 text boxes in a page. If you enter numbers in the
						text box and press one of the submit buttons, it will calculate and show the result according to
						the operation.</td>
					<td><a href="../Html Php/1-2.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>1-3</td>
					<td>Show the greatest common divisor.</td>
					<td>Display 2 text boxes in a page. If you enter numbers in the text box and press the submit
						button, it will calculate and show the result.</td>
					<td><a href="../Html Php/1-3.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>1-4</td>
					<td>Solve the FizzBuzz problem.</td>
					<td>"Display 1 text box in a page. If you enter a number, which means last number, in the text box
						and press the submit button, it will calculate and show the result.Print each number from 1 up
						to the submitted number on a new line. For each multiple of 3, print ""Fizz"" instead of the
						number. For each multiple of 5, print ""Buzz"" instead of the number. For numbers which are
						multiples of both 3 and 5, print ""FizzBuzz"" instead of the number."</td>
					<td><a href="../Html Php/1-4.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>1-5</td>
					<td>Input date. Then show 3 days from the input date and the days of the week of each date.</td>
					<td>Display 1 text box in a page. If you enter the date in the text box and press the submit button,
						it will calculate and show the result.</td>
					<td><a href="../Html Php/1-5.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>1-6</td>
					<td>Input user information. Then show it on the next page.</td>
					<td>Create a form for basic user information. When you enter values in each input then press the
						submit button, it will show the inputted data in another page.</td>
					<td><a href="../Html Php/1-6.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>1-7</td>
					<td>Add validation in the user information form(required, numeric, character, mailaddress).</td>
					<td>"Validate inputted values in the form you created. Then show error messages if there is any
						error.Try to use regular expressions when you validate the mail address."</td>
					<td><a href="../Html Php/1-7.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>1-8</td>
					<td>Store inputted user information into a CSV file.</td>
					<td>CSV stands for comma separated value. Whenever the user information is submitted and validated,
						it will be appended at the bottom of the CSV file.</td>
					<td><a href="../Html Php/1-8.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>1-9</td>
					<td>Show the user information using table tags.</td>
					<td>Create a new page for listing up all the user information (from the csv) using table tags.</td>
					<td><a href="../Html Php/1-9.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>1-10</td>
					<td>Upload images.</td>
					<td>Add upload image feature for uploading user profile image.</td>
					<td><a href="../Html Php/1-10.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>1-11</td>
					<td>Show uploaded images in the table.</td>
					<td>Add uploaded images in the list page.</td>
					<td><a href="../Html Php/1-11.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>1-12</td>
					<td>Add pagination in the list page.</td>
					<td>Add pagination in the list page in order to show only 10 user information in each page.</td>
					<td><a href="../Html Php/1-12.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>1-13</td>
					<td>Create login form and embed it into the system that you developed.</td>
					<td>"Login form must have 2 text boxes for user ID and password. If there is any error in
						validation, the error message(s) will be shown.After login, user information will be saved in
						the session and keep login until the user logs out or closes the web browser."</td>
					<td><a href="../Html Php/1-13.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>


			</tbody>
		</table>
	</div>


	<div style="padding:50px;" class="div_javascript" hidden>
		<h1>Javascript </h1>
		<table class="table">
			<thead>
				<tr>
					<th width="70px">No.</th>
					<th>Title</th>
					<th>Description</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>2-1</td>
					<td>Show alert.</td>
					<td>Show alert when a page is ready.</td>
					<td><a href="../Javascript/2-1.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>2-2</td>
					<td>Confirm dialog and redirection</td>
					<td>"Create a page with a button. When you click the button, show the confirm dialog.If you press ok
						on the dialog, redirect to the other page. If you click cancel, remain on the page."</td>
					<td><a href="../Javascript/2-2.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>2-3</td>
					<td>The four basic operations of arithmetic</td>
					<td>Create 2 text boxes, 1 combo box and 1 submit button in order to input expressions and choose
						the way to calculate plus, minus, multiply, division.</td>
					<td><a href="../Javascript/2-3.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>2-4</td>
					<td>Show prime numbers.</td>
					<td>Create 1 text box and 1 submit button in order to display prime numbers from 2 to inputted
						numbers as maximum.</td>
					<td><a href="../Javascript/2-4.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>2-5</td>
					<td>Input characters in the textbox and show it in the label.</td>
					<td>Whenever you type a character in a text box, text in a label will be updated what you typed.
					</td>
					<td><a href="../Javascript/2-5.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>2-6</td>
					<td>Press the button and add a label below the button.</td>
					<td>Whenever you press a button, a new label will be appended below the button.</td>
					<td><a href="../Javascript/2-6.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>2-7</td>
					<td>Show alert when you click an image.</td>
					<td>Show alert when you click an image. Alert will tell you the file name of the image.</td>
					<td><a href="../Javascript/2-7.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>2-8</td>
					<td>Show alert when you click a link.</td>
					<td>Show alert when you click an image. Alert will tell you the url of the link.</td>
					<td><a href="../Javascript/2-8.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>2-9</td>
					<td>Change text and background color when you press buttons.</td>
					<td>Create some buttons that show the name of the color. When you click one of them, text or
						background color will be changed according to what you press.</td>
					<td><a href="../Javascript/2-9.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>2-10</td>
					<td>Scroll screen when you press buttons</td>
					<td>Create a page that is at least two times the screen height. There are 2 buttons located at the
						top of the page and at the bottom of the page. When you click the top button, the screen will
						scroll down to the bottom. Vice versa.</td>
					<td><a href="../Javascript/2-10.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>2-11</td>
					<td>Change background color using animation.</td>
					<td>Try to implement animation in order to change background color from dark blue to light blue
						using JavaScript.</td>
					<td><a href="../Javascript/2-11.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>2-12</td>
					<td>Show another image when you mouse over an image. Then show the original image when you mouse
						out.</td>
					<td>Prepare 2 images. Then show 1 of them in a page. When you mouse over the image, it will be
						changed with another image. Furthermore you mouse out from the image, it will be changed with
						the previous image.</td>
					<td><a href="../Javascript/2-12.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>2-13</td>
					<td>Change the size of images when you press buttons.</td>
					<td>Create 3 buttons below an image. These buttons must have "Small", "Medium" and "Large" labels.
						If you press any button, the size of the image will be transformed.</td>
					<td><a href="../Javascript/2-13.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>2-14</td>
					<td>Show images according to the options in the combo box.</td>
					<td>Prepare several images. Show one of them in a page and put a combo box beside the image. When
						you choose one of the options in the combo box, the image will be replaced with the other one.
					</td>
					<td><a href="../Javascript/2-14.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>2-15</td>
					<td>Show current date and time in real time.</td>
					<td>Put a label on a page. That label shows date and time. The label is refreshed every minute and
						show current date time.</td>
					<td><a href="../Javascript/2-15.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>


			</tbody>
		</table>
	</div>



	<div style="padding:50px;" class="div_database" hidden>
		<h1>Database </h1>
		<table class="table">
			<thead>
				<tr>
					<th width="70px">No.</th>
					<th>Title</th>
					<th>Description</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>3-1</td>
					<td>Install phpMyAdmin</td>
					<td>Install phpMyAdmin and try to access it in the browser.</td>
					<td><a href="../Database/3-1.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>3-2</td>
					<td>CREATE TABLE</td>
					<td>Create any table using SQL.</td>
					<td><a href="../Database/3-2.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>3-3</td>
					<td>INSERT, UPDATE, DELETE</td>
					<td>Try inserting, updating and deleting from the table you created.</td>
					<td><a href="../Database/3-3.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>3-4</td>
					<td>Solve problems using SQL</td>
					<td>See SQL Problems sheet.</td>
					<td><a href="../Database/3-4.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>3-5</td>
					<td>Use the database in the applications that you developed.</td>
					<td>Same as exercises 1-6 to 1-13 but instead of csv, use the database.</td>
					<td><a href="../Database/3-5.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>




			</tbody>
		</table>
	</div>


	<div style="padding:50px;" class="div_advance" hidden>
		<h1>Advanced SQL</h1>
		<table class="table">
			<thead>
				<tr>
					<th width="70px">No.</th>
					<th>Title</th>
					<th>Description</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>4-1</td>
					<td>Selecting by age using birth date</td>
					<td>Using same data as SQL problems. Get employees who is older than 30 years old but younger than
						40 years old. (Result may vary based on current date)</td>
					<td><a href="../Advanced Sql/4-1.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>4-2</td>
					<td>List all therapists according to their daily work shifts</td>
					<td>There are 5 therapists name John, Arnold, Robert, Ervin and Smith who are working in a Massage
						Company with a schedule from 6am in the morning up to 05:59am in the next morning.</td>
					<td><a href="../Advanced Sql/4-2.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>4-3</td>
					<td>Using Case conditional statement</td>
					<td>"Using same data as SQL problems. Get employees information with full position name. CEO = Chief
						Executive Officer CTO = Chief Technical Officer CFO = Chief Financial Officer Other position
						will remain the same"</td>
					<td><a href="../Advanced Sql/4-3.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>





			</tbody>
		</table>
	</div>

	<div style="padding:50px;" class="div_practice" hidden>
		<h1>Practice </h1>
		<table class="table">
			<thead>
				<tr>
					<th width="70px">No.</th>
					<th>Title</th>
					<th>Description</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>6-1</td>
					<td>Quiz with three multiple choices</td>
					<td>"Provide 10 questions. Show the score after.Questions should be stored in the database and
						should be randomized.Create wireframe and schema before developing, put them in [Your Name]
						(Quiz) sheet"</td>
					<td><a href="../Practice/6-1/" class="btn btn-primary">Go to this exercise</a></td>
				</tr>
				<tr>
					<td>6-2</td>
					<td>Calendar</td>
					<td>"Display calendar of specific month. Provides 2 buttons for forwarding and backwarding
						month.Default month displayed should be the current month and should highlight the current
						date."</td>
					<td><a href="../Practice/6-2.php" class="btn btn-primary">Go to this exercise</a></td>
				</tr>




			</tbody>
		</table>
	</div>

	<script type="text/javascript">
		$(".html_php").on('click', function() {
			$(".div_html_php").attr("hidden", false);
			$(".div_database").attr("hidden", true);
			$(".div_advance").attr("hidden", true);
			$(".div_practice").attr("hidden", true);
			$(".div_javascript").attr("hidden", true);
			$(".javscript_exe").removeClass("active");
			$(".html_php").addClass("active");
			$(".practice").removeClass("active");
			$(".database").removeClass("active");
			$(".sql").removeClass("active");
		})

		$(".javscript_exe").on('click', function() {
			$(".div_html_php").attr("hidden", true);
			$(".div_database").attr("hidden", true);
			$(".div_advance").attr("hidden", true);
			$(".div_practice").attr("hidden", true);
			$(".div_javascript").attr("hidden", false);
			$(".javscript_exe").addClass("active");
			$(".html_php").removeClass("active");
			$(".practice").removeClass("active");
			$(".database").removeClass("active");
			$(".sql").removeClass("active");
		})

		$(".database").on('click', function() {
			$(".div_html_php").attr("hidden", true);
			$(".div_database").attr("hidden", false);
			$(".div_advance").attr("hidden", true);
			$(".div_practice").attr("hidden", true);
			$(".div_javascript").attr("hidden", true);
			$(".javscript_exe").removeClass("active");
			$(".html_php").removeClass("active");
			$(".practice").removeClass("active");
			$(".database").addClass("active");
			$(".sql").removeClass("active");
		})


		$(".practice").on('click', function() {
			$(".div_html_php").attr("hidden", true);
			$(".div_database").attr("hidden", true);
			$(".div_advance").attr("hidden", true);
			$(".div_practice").attr("hidden", false);
			$(".div_javascript").attr("hidden", true);
			$(".javscript_exe").removeClass("active");
			$(".html_php").removeClass("active");
			$(".practice").addClass("active");
			$(".sql").removeClass("active");

		})

		$(".sql").on('click', function() {
			$(".div_html_php").attr("hidden", true);
			$(".div_database").attr("hidden", true);
			$(".div_advance").attr("hidden", false);
			$(".div_practice").attr("hidden", true);
			$(".div_javascript").attr("hidden", true);
			$(".javscript_exe").removeClass("active");
			$(".html_php").removeClass("active");
			$(".practice").removeClass("active");
			$(".sql").addClass("active");

		})
	</script>

</body>

</html>