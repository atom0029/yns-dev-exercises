<!DOCTYPE html>
<html>

<head>
	<title></title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<style type="text/css">
		table {
			border: 1px solid #aaa;
			border-collapse: collapse;
			background-color: #fff;
			font-family: Verdana;
			font-size: 20px;
			margin-left: 20px;
		}

		th {
			background-color: #777;
			color: #fff;
			height: 32px;
		}

		td {
			border: 1px solid #ccc;
			height: 70px;
			width: 100px;
			text-align: center;
		}

		td.red {
			color: red;
		}

		td.bg-yellow {
			background-color: #ffffe0;
		}

		td.bg-orange {
			background-color: #ffa500;
		}

		td.bg-green {
			background-color: #90ee90;
		}

		td.bg-white {
			background-color: #fff;
		}

		td.bg-blue {
			background-color: #add8e6;
		}

		a {
			color: #333;
			text-decoration: none;

		}
	</style>
</head>

<body>


	<table style="margin-left: 20px">
		<div id="calendar" class="right">
			<?php
            // $date = time ();
            isset($_GET['date']) ? $date = strtotime($_GET['date'] . '-01') :  $date = date(time());

            $prev = date('Y-m', mktime(0, 0, 0, date('m', $date)-1, date('d'), date('Y', $date)));
            $next = date('Y-m', mktime(0, 0, 0, date('m', $date)+1, date('d'), date('Y', $date)));
        
            $day = date('d', $date) ;
            $month = date('m', $date) ;
            $year = date('Y', $date) ;
        
            $first_day = mktime(0, 0, 0, $month, 1, $year) ;
            $title = date('F', $first_day) ;
            $day_of_week = date('D', $first_day) ;
            
            switch ($day_of_week) {
                case "Sun": $blank = 0; break;
                case "Mon": $blank = 1; break;
                case "Tue": $blank = 2; break;
                case "Wed": $blank = 3; break;
                case "Thu": $blank = 4; break;
                case "Fri": $blank = 5; break;
                case "Sat": $blank = 6; break;
            }
            
            $days_in_month = date('t', mktime(0, 0, 0, $month, 1, $year));
        ?>

			<div id="php-calendar" class="right">
				<table>
					<tr>
						<th colspan='7'><?php echo $title. " ".$year; ?>
						</th>
					</tr>
					<tr>
						<td width=42>S</td>
						<td width=42>M</td>
						<td width=42>T</td>
						<td width=42>W</td>
						<td width=42>T</td>
						<td width=42>F</td>
						<td width=42>S</td>
					</tr>

					<?php
         $day_count = 1;
        
         echo "<tr>";
        
         while ($blank > 0) {
             echo "<td></td>";
             $blank = $blank-1;
             $day_count++;
         }
        
         $day_num = 1;
        
         while ($day_num <= $days_in_month) {
             if ($month == date('m') && $year == date('Y') && $day_num == date('d')) {
                 echo "<td style='background-color: gray'> $day_num</td>";
             } else {
                 echo "<td >$day_num</td>";
             }
             $day_num++;
             $day_count++;
        
             if ($day_count > 7) {
                 echo "</tr><tr>";
                 $day_count = 1;
             }
         }
        
         while ($day_count >1 && $day_count <=7) {
             echo "<td> </td>";
             $day_count++;
         }
         echo '<a class="btn btn-danger" style="margin: 20px" href="6-2.php?date='.$prev.'"> Prev  </a>';
         echo '<a class="btn btn-primary" style="margin: 20px" href="6-2.php?date='.$next.'"> Next  </a>';
         ?>

				</table>
			</div>



</body>

</html>