<?php
require_once 'proccess.php';

if (isset($_SESSION['user'])) {
} else {
    header("Location: index.php");
}

if (isset($_POST['btn_logout'])) {
    session_destroy();
    header("Location: index.php");
}

?>

<!DOCTYPE html>
<html>

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style>
    body {
      margin: 0;
      font-family: Arial, Helvetica, sans-serif;
    }

    .topnav {
      overflow: hidden;
      background-color: #333;
    }

    .topnav a {
      float: left;
      color: #f2f2f2;
      text-align: center;
      padding: 14px 16px;
      text-decoration: none;
      font-size: 17px;
    }

    .topnav a:hover {
      background-color: #ddd;
      color: black;
    }

    .topnav a.active {
      background-color: #04AA6D;
      color: white;
    }
  </style>
</head>

<body>

  <div class="topnav">
    <a class="" href="home.php">Home</a>
    <a href="make-quiz.php">Make a quiz</a>
    <form method="post">
      <button type="submit" class="btn btn-danger" name="btn_logout"
        style="position: absolute; right: 0;margin: 10px">Logout</button>
    </form>
  </div>



  <div style="padding:50px">


    <div style="padding:50px">


      <?php
        $query = "Select * from quiz where id  = ".$_GET['id']."";
        $quiz = $con->query($query);
        foreach ($quiz as $info) {
            echo "<h3>Quiz Name: ".$info['name']." <h3>";
            echo "<h6>Description: ".$info['description']." <h6>";
        }
    ?>

      <?php
      $query = "Select * from quiz_question where quiz_id = ".$_GET['id']." order by rand()";
      $questions = $con->query($query);
      $i = 1;
      $correct_answer = [];
      foreach ($questions as $question):

        ?>
      <input type="hidden" name="" class="qid"
        value="<?php echo $question['id'] ?>">
      <h3 style="margin-top: 20px; margin-left:20px;text-transform: uppercase ;"><?php echo $i.'. '. $question['question']; ?>
      </h3> <br>
      <?php
        $query = "Select * from quiz_question_answer where question_id =  '".$question['id']."' order by rand()";
        $answers = $con->query($query);
        foreach ($answers as $answer):
          if ($answer['is_correct'] == 1) {
              array_push($correct_answer, $answer['id']);
          }
          ?>
      <input style="margin-left: 50px" type="radio"
        class="question_<?php echo $question['id'] ?>"
        id="<?php echo $answer['answer'] . $question['id'] ?>"
        name="<?php echo $question['id'] ?>"
        value="<?php echo $answer['id'] ?>">
      <label
        for="<?php echo $answer['answer'] . $question['id'] ?>"><?php echo $answer['answer'] ?></label> <br>
      <?php  endforeach; ?>
      <?php $i+=1; endforeach; ?>

      <button id="btn_finish" class="btn btn-primary " style="margin: 20px">Submit</button>

    </div>

  </div>

  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">Result</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <h1 class="user_score"></h1>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $("#btn_finish").on('click', function() {

      var correct = <?php echo json_encode($correct_answer); ?> ;
      var user_correct = 0;
      var flag = 0;
      questionNo = 1;
      question = [];
      answer = [];
      $(".qid").each(function(i) {
        flag = 0
        question.push(this.value)
        $(".question_" + this.value + "").each(function(i) {
          if ($(this).is(':checked')) {
            answer.push(this.value)
            flag = 1;
          }
        });
        if (!flag)
          alert('Question no ' + questionNo + ' must be answered')
        questionNo++;
      });
      console.log(question)
      console.log(answer)
      console.log(correct)
      for (var i = 0; i < correct.length; i++) {
        if (correct[i] == answer[i])
          user_correct += 1;
      }
      if (flag) {
        $(".user_score").text('Score: ' + user_correct + '/' + correct.length)
        $("#myModal").modal("show");
      }
    })
  </script>



</body>

</html>