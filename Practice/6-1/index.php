<?php
  require_once 'proccess.php';
?>

<!DOCTYPE html>
<html>

<head>
  <title></title>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <link href="asset/login.css" rel="stylesheet">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body>


  <div class="wrapper fadeInDown">

    <div id="formContent">
      <h2 class="active h2_sign_in"> Sign In </h2>
      <h2 class=" h2_sign_up">Sign Up </h2>
      <div class="fadeIn first">
        <img src="asset/images/user.png" alt="User Icon" style="max-height: 50px;max-width: 50px" />
      </div>


      <?php if (isset($emailExist)): ?>
      <div class="alert alert-danger" role="alert">
        <?= $emailExist  ?>
      </div>
      <?php endif;  ?>
      <form method="POST" class="form_sign_in">
        <input type="email" class="fadeIn second" name="username" placeholder="Email">
        <input type="password" class="fadeIn third" name="password" placeholder="Password">
        <input type="submit" class="fadeIn fourth btn_log_in" name="btn_login" value="Log In">
      </form>


      <form method="POST" hidden class="form_sign_up" hidden>
        <input type="text" class="fadeIn second" name="firstname" placeholder="Fist name" value="<?php if (isset($firstname)) { echo $firstname; } ?>">
        <input type="text" class="fadeIn second" name="middlename" placeholder="Middle name" value="<?php if (isset($lastname)) { echo $lastname; } ?>">
        <input type="text" class="fadeIn second" name="lastname" placeholder="Last name" value="<?php if (isset($middlename)) { echo $middlename; } ?>">
        <input type="email" class="fadeIn second" name="signup_email" placeholder="Email" value="<?php if (isset($email)) { echo $email; } ?>">
        <input type="password" class="fadeIn third" name="signup_password" placeholder="Password">
        <button type="submit" class="btn btn-default" name="btn_sign_up">Sign up</button>
      </form>


    </div>
  </div>

  <script type="text/javascript">
    $(".h2_sign_up").click(function() {
      $(".form_sign_up").attr("hidden", false);
      $(".form_sign_in").attr("hidden", true);
      $(".h2_sign_in").removeClass("active")
      $(".h2_sign_up").addClass("active")

    })

    $(".h2_sign_in").click(function() {
      $(".form_sign_up").attr("hidden", true);
      $(".form_sign_in").attr("hidden", false);
      $(".h2_sign_up").removeClass("active")
      $(".h2_sign_in").addClass("active")

    })
    
    <?php if (isset($emailExist)): ?>
      $(".form_sign_up").attr("hidden", false);
      $(".form_sign_in").attr("hidden", true);
      $(".h2_sign_in").removeClass("active")
      $(".h2_sign_up").addClass("active")
    <?php endif;  ?>
  </script>
</body>

</html>