<?php
require_once 'proccess.php';

if (isset($_SESSION['user'])) {
} else {
    header("Location: index.php");
}

if (isset($_POST['btn_logout'])) {
    session_destroy();
    header("Location: index.php");
}

?>

<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" >

  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <style>
    body {
      margin: 0;
      font-family: Arial, Helvetica, sans-serif;
    }

    .topnav {
      overflow: hidden;
      background-color: #333;
    }

    .topnav a {
      float: left;
      color: #f2f2f2;
      text-align: center;
      padding: 14px 16px;
      text-decoration: none;
      font-size: 17px;
    }

    .topnav a:hover {
      background-color: #ddd;
      color: black;
    }

    .topnav a.active {
      background-color: #04AA6D;
      color: white;
    }
  </style>
</head>
<body>

  <div class="topnav">
    <a class="" href="home.php">Home</a>
    <a href="make-quiz.php" class="active">Make a quiz</a>
    <form method="post">
      <button type="submit" class="btn btn-danger" name="btn_logout" style="position: absolute; right: 0;margin: 10px">Logout</button>
    </form>
  </div>



  <div style="padding:50px">


    <div style="padding:50px">
      <h1> Make Quiz </h1>


      <div class="form-group">
        <label for="username">Quiz Name: *</label>
        <input type="text" class="form-control txt_quiz_name" name="txt_quiz_name">
      </div>
      <div class="form-group">
        <label for="text">Quiz Description: *</label>
        <input type="text" class="form-control txt_quiz_description" name="txt_quiz_description">
      </div>

      <div class="form-group">
        <label for="text">Question: *</label>
        <input type="text" class="form-control txt_question" name="txt_quiz_question" required>
      </div>
      <div class="form-group">
        <label for="text">Choices: *</label>
        <input type="text" class="form-control txt_choice" name="txt_quiz_question" required>
        <input type="text" class="form-control txt_choice" name="txt_quiz_question" required>
        <input type="text" class="form-control txt_choice" name="txt_quiz_question" required>
        <input type="text" class="form-control txt_choice" name="txt_quiz_question" required>
      </div>

      <div class="form-group">

        <label for="text">Correct Answer: *</label>
        <button class="btn btn-default btn_get_choice">Load choices</button>
        <select class="sel_correct_answer form-control" disabled>

        </select>
      </div>

      <button type="button" class="btn btn-default btn_next" name="btn_next">Next</button>



      <button type="button" class="btn btn-default btn_submit" name="btn_submit" hidden>Submit</button>




    </div>

<script type="text/javascript">
  var i = 0;
  var isValid = 1;
  var isAnswerSelected = 0;
  question = [];
  answer = [];
  correct = [];
  var correct_answer = 0;
  $(".btn_get_choice").on('click',function(){
     checkTextbox()
    if(isValid == 0){
      isAnswerSelected = 0;
      alert('All fields is required')
    }
    else{
      $(".sel_correct_answer").attr("disabled",false);
      $(".sel_correct_answer").empty()

      $( ".txt_choice" ).each(function() {

          $(".sel_correct_answer").append("<option value="+correct_answer+"> "+$(this).val()+" </option>")
          correct_answer++;
      });
      isAnswerSelected = 1;
    }

    
  })

  function checkTextbox(){
    isValid = 1;
    if($('.txt_quiz_name').val() == '' || $('.txt_quiz_description').val() == '' || $('.txt_question').val() == '' ){
      isValid = 0;
    }
    $( ".txt_choice" ).each(function() {
      if(this.value == '')
        isValid = 0;
    });

  }


  $(".btn_next").on('click',function(){
    checkTextbox()
    if(isValid == 0){
      isAnswerSelected = 0;
      alert('All fields is required')
    }
    else   if(isAnswerSelected == 0){
      alert('Correct answer must be selected');
    }
    else if(isValid == 1 ){
      correct.push($( ".sel_correct_answer option:selected" ).val())
      question.push($(".txt_question").val())
      $(".txt_question").val('')
      $(".sel_correct_answer").empty()
      $( ".txt_choice" ).each(function() {
        answer.push(this.value)
        this.value = '';
      });
      $(".sel_correct_answer").attr("disabled",true);
      isAnswerSelected = 0;
      i+=1;
      if(i==9){
        $(".btn_next").attr("hidden",true);
        $(".btn_submit").attr("hidden",false);
      }
    }

  })

  $(".btn_submit").on('click',function(){
    checkTextbox()
    if(isValid == 0){
      isAnswerSelected = 0;
      alert('All fields is required')
    }
    else if(isAnswerSelected == 0){
      alert('Correct answer must be selected');
    }
    else if(isValid == 1 ){
      quizName = $(".txt_quiz_name").val() 
      quizDescription = $(".txt_quiz_description").val()
      correct.push($( ".sel_correct_answer option:selected" ).val())
      question.push($(".txt_question").val())
      $(".txt_question").val('')
      $( ".txt_choice" ).each(function() {
        answer.push(this.value)
        this.value = '';
      });
      $.ajax({
        url: 'proccess.php',
        type: 'post',
        data: { "makeQuiz": "1", question: question, answer: answer, quizName: quizName, quizDescription: quizDescription,correct: correct},
        success: function(response) { location.href = 'home.php' },
        error: function(error) { console.log(error); }
      });
    }
  })
</script>
</body>
</html>
