-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql-server
-- Generation Time: Jan 12, 2022 at 07:47 AM
-- Server version: 8.0.19
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `practice`
--

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE `quiz` (
  `id` int NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(150) NOT NULL,
  `created_by` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`id`, `name`, `description`, `created_by`) VALUES
(12, 'General Knowledge', 'Technology', 14);

-- --------------------------------------------------------

--
-- Table structure for table `quiz_question`
--

CREATE TABLE `quiz_question` (
  `id` int NOT NULL,
  `quiz_id` int NOT NULL,
  `question` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `quiz_question`
--

INSERT INTO `quiz_question` (`id`, `quiz_id`, `question`) VALUES
(18, 12, 'In which decade was the American Institute of Electrical Engineers (AIEE) founded?'),
(19, 12, 'What is part of a database that holds only one type of information?'),
(20, 12, '\'OS\' computer abbreviation usually means ?'),
(21, 12, 'In which decade with the first transatlantic radio broadcast occur?'),
(22, 12, '\'.MOV\' extension refers usually to what kind of file?'),
(23, 12, 'What frequency range is the High Frequency band?'),
(24, 12, 'The first step to getting output from a laser is to excite an active medium. What is this process called?'),
(25, 12, 'What is the relationship between resistivity r and conductivity s?'),
(26, 12, 'Which motor is NOT suitable for use as a DC machine?'),
(27, 12, 'A given signal\'s second harmonic is twice the given signal\'s __________ frequency...?');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_question_answer`
--

CREATE TABLE `quiz_question_answer` (
  `id` int NOT NULL,
  `question_id` int NOT NULL,
  `answer` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `is_correct` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `quiz_question_answer`
--

INSERT INTO `quiz_question_answer` (`id`, `question_id`, `answer`, `is_correct`) VALUES
(42, 18, '1870s', 0),
(43, 18, '1880s', 1),
(44, 18, '1930s', 0),
(45, 18, '1950s', 0),
(46, 19, 'Report', 0),
(47, 19, 'Field', 1),
(48, 19, 'Record', 0),
(49, 19, 'File', 0),
(50, 20, 'Order of Significance', 0),
(51, 20, 'Open Software', 0),
(52, 20, 'Operating System', 1),
(53, 20, 'Optical Sensor', 0),
(54, 21, '1850s', 0),
(55, 21, '1860s', 0),
(56, 21, '1870s', 0),
(57, 21, '1900s', 1),
(58, 22, 'Image file', 0),
(59, 22, 'Animation/movie file', 1),
(60, 22, 'Audio file', 0),
(61, 22, 'MS Office document', 0),
(62, 23, '100 kHz', 0),
(63, 23, '1 GHz', 0),
(64, 23, '30 to 300 MHz', 0),
(65, 23, '3 to 30 MHz', 1),
(66, 24, 'Pumping', 1),
(67, 24, 'Exciting', 0),
(68, 24, 'Priming', 0),
(69, 24, 'Raising', 0),
(70, 25, 'R = s2', 0),
(71, 25, 'R = s', 0),
(72, 25, 'R > s', 0),
(73, 25, 'R = 1/s', 1),
(74, 26, 'Permanent magnet motor', 0),
(75, 26, 'Series motor', 0),
(76, 26, 'Squirrel cage motor', 1),
(77, 26, 'Synchronous motor', 0),
(78, 27, 'Fourier', 0),
(79, 27, 'Foundation', 0),
(80, 27, 'Fundamental', 1),
(81, 27, 'Field', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `middlename` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `middlename`, `email`, `password`, `reg_date`) VALUES
(14, 'Anel Thom', '', 'Macalla', 'anelthom@gmail.com', '$2y$10$2eVEaNG3wO4EbbDHalykI.Solk1xtO5.6pkGXjrwyvK77UkXdVjPq', '2022-01-12 07:29:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_fk_users_id` (`created_by`);

--
-- Indexes for table `quiz_question`
--
ALTER TABLE `quiz_question`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_fk_quiz_id` (`quiz_id`);

--
-- Indexes for table `quiz_question_answer`
--
ALTER TABLE `quiz_question_answer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_fk_question_id` (`question_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `quiz`
--
ALTER TABLE `quiz`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `quiz_question`
--
ALTER TABLE `quiz_question`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `quiz_question_answer`
--
ALTER TABLE `quiz_question_answer`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `quiz`
--
ALTER TABLE `quiz`
  ADD CONSTRAINT `emp_fk_users_id` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `quiz_question`
--
ALTER TABLE `quiz_question`
  ADD CONSTRAINT `emp_fk_quiz_id` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`id`);

--
-- Constraints for table `quiz_question_answer`
--
ALTER TABLE `quiz_question_answer`
  ADD CONSTRAINT `emp_fk_question_id` FOREIGN KEY (`question_id`) REFERENCES `quiz_question` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;