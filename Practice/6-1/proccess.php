<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
    $con = new mysqli('mysql-server', 'root', 'secret', 'practice') or die(mysqli_error($con));
    if (session_status() === PHP_SESSION_NONE) {
        session_start();
    }

    if (isset($_POST['btn_sign_up'])) {
        $firstname  = $_POST["firstname"];
        $lastname  = $_POST["middlename"];
        $middlename  = $_POST["lastname"];
        $password = password_hash($_POST["signup_password"], PASSWORD_DEFAULT);
        $email = $_POST["signup_email"];
    
        $stmt = $con->prepare("SELECT count(*) exist from users where email = ?");
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $result = $stmt->get_result();
        $row = $result->fetch_assoc();
        if ($row['exist'] >= 1) {
            $emailExist =  "Email Already Exist";
        } else {
            $stmt = $con->prepare("INSERT INTO users (firstname,lastname,middlename,email,password) VALUES(?,?,?,?,?)");
            $stmt->bind_param('sssss', $firstname, $lastname, $middlename, $email, $password);
            echo $stmt->execute();
        }
    }

    if (isset($_POST['btn_login'])) {
        $user = $_POST['username'];
        $pass = $_POST['password'];
        $stmt = $con->prepare("Select count(id) user  from users where email = ?");
        $stmt->bind_param('s', $user);
        $stmt->execute();
        $result = $stmt->get_result();
        $row = $result->fetch_assoc();
        if ($row['user'] == 0) {
            $error = "Invalid User or password";
        } else {
            $stmt = $con->prepare("Select id,password  from users where email = ?");
            $stmt->bind_param('s', $user);
            $stmt->execute();
            $result = $stmt->get_result();
            $row = $result->fetch_assoc();
        
            if (password_verify($pass, $row['password'])) {
                $_SESSION['user'] = $row['id'];
                header("Location: home.php");
            } else {
                $error = "Invalid User or password";
            }
        }
    }



    if (isset($_POST['makeQuiz'])) {
        $name  = $_POST["quizName"];
        $desc  = $_POST["quizDescription"];
        $i = 0;
        $iteration = 0;

        $stmt = $con->prepare("INSERT INTO quiz (name,description,created_by) VALUES(?,?,?)");
        $stmt->bind_param('ssi', $name, $desc, $_SESSION['user']);
        $stmt->execute();
        $quizId = $con->insert_id;
        foreach ($_POST["question"] as $question) {
            $stmt = $con->prepare("INSERT INTO quiz_question (quiz_id,question) VALUES(?,?)");
            $stmt->bind_param('is', $quizId, $question);
            $stmt->execute();
            $question_id = $con->insert_id;
            for ($j = $i; $j < $i + 4; $j++) {
                if ($_POST["correct"][$iteration] == $j) {
                    $is_correct = 1;
                } else {
                    $is_correct = 0;
                }
                $stmt = $con->prepare("INSERT INTO quiz_question_answer (question_id,answer,is_correct) VALUES(?,?,?)");
                $stmt->bind_param('isi', $question_id, $_POST["answer"][$j], $is_correct);
                $stmt->execute();
                $answer_id = $con->insert_id;
            }
            $iteration += 1;
            $i+=4;
        }
        echo "Insert Success";
        ;
    }
