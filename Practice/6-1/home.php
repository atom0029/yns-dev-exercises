<?php
    require_once 'proccess.php';

    if (isset($_SESSION['user'])) {
    } else {
        header('Location: index.php');
    }

    if (isset($_POST['logout'])) {
        session_destroy();
        header('Location: index.php');
    }
?>

<!DOCTYPE html>
<html>

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<style>
		body {
			margin: 0;
			font-family: Arial, Helvetica, sans-serif;
		}

		.topnav {
			overflow: hidden;
			background-color: #333;
		}

		.topnav a {
			float: left;
			color: #f2f2f2;
			text-align: center;
			padding: 14px 16px;
			text-decoration: none;
			font-size: 17px;
		}

		.topnav a:hover {
			background-color: #ddd;
			color: black;
		}

		.topnav a.active {
			background-color: #04AA6D;
			color: white;
		}
	</style>
</head>

<body>
	<div class="topnav">
		<a class="active" href="home.php">Home</a>
		<a href="make-quiz.php">Make a quiz</a>
		<form method="post">
			<button type="submit" class="btn btn-danger" name="logout"
				style="position: absolute; right: 0;margin: 10px">Logout</button>
		</form>
	</div>
	<div style="padding:50px">uiz </h1>
		<table class="table">
			<thead>
				<tr>
					<th>Quiz Name</th>
					<th>Description</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php
		            $query = "Select * from quiz";
		            $quizzes = $con->query($query);
		            $i = 1;
		            foreach ($quizzes as $quiz):
		        ?>
				<tr>
					<td><?= $quiz['name']; ?></td>
					<td><?= $quiz['description']; ?></td>
					<td><a href="quiz.php?id=<?= $quiz['id'] ?>" class="btn btn-primary">Take quiz</a></td>
				</tr>
				<?php  endforeach; ?>

			</tbody>
		</table>
	</div>

</body>

</html>