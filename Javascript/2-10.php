<!DOCTYPE html>
<html>

<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<style type="text/css">
		html,
		body {
			height: 150%;
		}
	</style>
</head>

<body>
	<button onclick="scrollBottom()">Go to Bottom</button>

	<button onclick="scrollBTop()" style="display: none;position: fixed;bottom: 0;z-index: 99;margin: 10px;"
		id="goToTop">Go to Top</button>



<script type="text/javascript">
	function scrollBottom() {
		window.scrollTo(0, document.body.scrollHeight);
	}

	var btn = document.getElementById('goToTop')
	window.onscroll = function() {
		scrollFunction()
	};

	function scrollFunction() {
		if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
			btn.style.display = 'block';
		} else {
			btn.style.display = 'none';
		}
	}

	// When the user clicks on the button, scroll to the top of the document
	function scrollBTop() {
		document.body.scrollTop = 0;
		document.documentElement.scrollTop = 0;
	}
</script>

</body>

</html>