<!DOCTYPE html>
<html>

<head>
<title></title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<style type="text/css">
	@keyframes bodyChangeBackground {
		from {
			background-color: darkblue;
		}
		to {
			background-color: blue;
		}
	}

	@keyframes buttonChangeColor {
		from {
			color: aqua;
		}
		to {
			color: red;
		}
	}
</style>
</head>

<body id="body">

	<button id="change" style="margin: 20px">Change Color Animation</button>

	<script type="text/javascript">
		document.getElementById('change').addEventListener('click', function() {
			document.getElementById('change').style.animation = "buttonChangeColor 2s infinite";
			document.getElementById("body").style.animation = "bodyChangeBackground 2s infinite";
		})
	</script>

</body>

</html>