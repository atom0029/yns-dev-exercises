<!DOCTYPE html>
<html>

<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
	<div style="padding: 20px;" id="main_div">
		<img src="image/test.gif" id="imageChange" onmouseover="changeImage()" onmouseout="changeBackImage()"
			style="width: 80%; height: 80vh; border: solid">
	</div>

<script type="text/javascript">
	function changeImage() {
		document.getElementById('imageChange').src = 'image/run.gif';
	}

	function changeBackImage() {
		document.getElementById('imageChange').src = 'image/test.gif';
	}
</script>
</body>

</html>