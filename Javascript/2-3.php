<!DOCTYPE html>
<html>

<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

</head>

<body>
	<div style="padding: 20px">
		<h2 id="result"></h2>
		<div class="form-group">
			<label for="operation">Operation:</label>
			<select class="form-control" id="operation">
				<option>Addtion</option>
				<option>Subtraction</option>
				<option>Multiplication</option>
				<option>Division</option>
			</select>
		</div>
		<div class="form-group">
			<label for="num1">Number 1:</label>
			<input type="number" step="0.0001" class="form-control" id="num1">
		</div>
		<div class="form-group">
			<label for="num2">Number 2:</label>
			<input type="number" step="0.0001" class="form-control" id="num2">
		</div>

		<button type="submit" class="btn btn-default" id="btn_submit" onclick="calculate()">Submit</button>
	</div>
	<script type="text/javascript">
		function calculate() {
			$num1 = document.getElementById('num1').value
			$num2 = document.getElementById('num2').value
			$operation = document.getElementById('operation').value

			switch ($operation) {
				case 'Addtion':
					$result = parseFloat($num1) + parseFloat($num2);
					document.getElementById('result').innerHTML = 'Result: ' + $result
					break;
				case 'Subtraction':
					$result = parseFloat($num1) - parseFloat($num2);
					document.getElementById('result').innerHTML = 'Result: ' + $result
					break;
				case 'Multiplication':
					$result = parseFloat($num1) * parseFloat($num2);
					document.getElementById('result').innerHTML = 'Result: ' + $result
					break;
				case 'Division':
					if ($num1 == 0 && $num2 == 0)
						document.getElementById('result').innerHTML = 'Invalid Input'
					else if ($num2 == 0)
						document.getElementById('result').innerHTML = 'Invalid Input'
					else {
						$result = parseInt($num1) / parseInt($num2);
						document.getElementById('result').innerHTML = 'Result: ' + $result
					}
					break;
				default:
					document.getElementById('result').innerHTML = 'Something went wrong'

			}


		}
	</script>

</body>

</html>