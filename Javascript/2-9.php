<!DOCTYPE html>
<html>

<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
	<div style="padding: 20px" id="main_div">
		<button class="btn-default" id="red">Red</button>
		<button class="btn-default" id="green">Green</button>
		<button class="btn-default" id="blue">Blue</button>
		<button class="btn-default" id="black">Black</button>
	</div>

	<script type="text/javascript">
		document.getElementById('red').addEventListener('click', function() {
			document.getElementById('red').style.backgroundColor = this.innerHTML;
			document.getElementById('green').style.backgroundColor = this.innerHTML;
			document.getElementById('blue').style.backgroundColor = this.innerHTML;
			document.getElementById('black').style.backgroundColor = this.innerHTML;
		})

		document.getElementById('green').addEventListener('click', function() {
			document.getElementById('red').style.backgroundColor = this.innerHTML;
			document.getElementById('green').style.backgroundColor = this.innerHTML;
			document.getElementById('blue').style.backgroundColor = this.innerHTML;
			document.getElementById('black').style.backgroundColor = this.innerHTML;
		})

		document.getElementById('blue').addEventListener('click', function() {
			document.getElementById('red').style.backgroundColor = this.innerHTML;
			document.getElementById('green').style.backgroundColor = this.innerHTML;
			document.getElementById('blue').style.backgroundColor = this.innerHTML;
			document.getElementById('black').style.backgroundColor = this.innerHTML;
		})
		document.getElementById('black').addEventListener('click', function() {
			document.getElementById('red').style.backgroundColor = this.innerHTML;
			document.getElementById('green').style.backgroundColor = this.innerHTML;
			document.getElementById('blue').style.backgroundColor = this.innerHTML;
			document.getElementById('black').style.backgroundColor = this.innerHTML;
		})
	</script>

</body>

</html>