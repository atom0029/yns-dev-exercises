<!DOCTYPE html>
<html>

<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
	<div style="padding: 20px; " id="main_div">
		<img src="image/test.gif" id="imageChange" style="width: 70%; height: 80vh; border: solid">
		<select id="imgChange" class="form-control" onchange="changeIMG()">

			<option value="test.gif">Sing</option>
			<option value="bird.gif">Bird</option>
			<option value="mario.gif">Mario</option>
			<option value="picachu.gif">Picachu</option>
			<option value="run.gif">Run</option>
		</select>
	</div>

<script type="text/javascript">
	function changeIMG() {
		document.getElementById('imageChange').src = "image/" + document.getElementById('imgChange').value;
	}
</script>
</body>

</html>