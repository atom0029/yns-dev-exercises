<!DOCTYPE html>
<html>

<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
	<div style="padding: 20px" id="mainDiv">


		<button type="submit" class="btn btn-default" id="btn_submit" onclick="generateLabel()">Create Label</button>
	</div>

	<script type="text/javascript">
		function generateLabel() {
			let div = document.getElementById('mainDiv')
			let lineBreak = document.createElement('br');
			let label = document.createElement('label');
			label.innerHTML = 'Label Created'
			div.append(lineBreak)
			div.append(label)
		}
	</script>

</body>

</html>