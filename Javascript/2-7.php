<!DOCTYPE html>
<html>

<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
	<div style="padding: 20px" id="main_div">
		<img src="image/test.gif" id="img_test">
	</div>

	<script type="text/javascript">
		document.getElementById('img_test').addEventListener('click', function() {
			var fullPath = document.getElementById('img_test').src;
			var filename = fullPath.replace(/^.*[\\\/]/, '');
			alert(filename)
		});
	</script>

</body>

</html>