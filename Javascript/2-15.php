<!DOCTYPE html>
<html>

<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
	<div style="padding: 20px">
		<h3>Time</h3>
		<label id="time"></label>
	</div>

<script type="text/javascript">
	document.getElementById('time').innerHTML = new Date().toLocaleString()
	setInterval(function() {
		document.getElementById('time').innerHTML = new Date().toLocaleString()
	}, 1000);
</script>
</body>

</html>