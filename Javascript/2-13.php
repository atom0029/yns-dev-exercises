<!DOCTYPE html>
<html>

<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
	<div style="padding: 20px; " id="main_div">
		<img src="image/test.gif" id="imgChange" onmouseover="changeImage()" onmouseout="changeBackImage()"
			style="width: 80%; height: 80vh; border: solid"><br>
		<div style="padding: 20px">
			<button id="btn_small" onclick="resizeSmall()">Small</button>
			<button id="btn_medium" onclick="resizeMedium()">Medium</button>
			<button id="btn_large" onclick="resizeLarge()">Large</button>
		</div>
	</div>

<script type="text/javascript">
	function changeImage() {
		document.getElementById('imgChange').src = 'image/run.gif';
	}

	function changeBackImage() {
		document.getElementById('imgChange').src = 'image/test.gif';
	}

	function resizeSmall() {
		document.getElementById('imgChange').style.width = '30%';
		document.getElementById('imgChange').style.height = '30vh';
	}

	function resizeMedium() {
		document.getElementById('imgChange').style.width = '50%';
		document.getElementById('imgChange').style.height = '50vh';
	}

	function resizeLarge() {
		document.getElementById('imgChange').style.width = '80%';
		document.getElementById('imgChange').style.height = '80vh';
	}
</script>

</body>

</html>