<!DOCTYPE html>
<html>

<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
	<div style="padding: 20px">
		<h5 id="result"></h5>
		<div class="form-group">
			<label for="num1">Number:</label>
			<input type="number" step="0.0001" class="form-control" id="num1">
		</div>


		<button type="submit" class="btn btn-default" id="btn_submit" onclick="calculate()">Submit</button>
	</div>

	<script type="text/javascript">
		function calculate() {
			$num1 = document.getElementById('num1').value
			if ($num1 < 1)
				document.getElementById('result').innerHTML = 'Negative Numbers is not prime number'
			else if (isNaN($num1))
				document.getElementById('result').innerHTML = 'Please Enter Number'
			else {
				$prime_numbers = [];
				for (var i = 2; i <= $num1; i++) {
					$prime = 1;
					for (var j = 2; j < i; j++) {
						if (i % j == 0) {
							$prime = 0;
							break;
						}
					}
					if ($prime == 1) {
						$prime_numbers.push(i)
					}

				}
				document.getElementById('result').innerHTML = 'Prime Numbers: ' + $prime_numbers
			}
		}
	</script>

</body>

</html>