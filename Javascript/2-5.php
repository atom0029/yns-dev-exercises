<!DOCTYPE html>
<html>

<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
	<div style="padding: 20px">
		<h5 id="result"></h5>
		<div class="form-group">
			<label for="txt_num1">Text:</label>
			<input type="text" step="0.0001" class="form-control" id="txt_any" onKeyPress="edValueKeyPress()"
				onKeyUp="edValueKeyPress()">
		</div>

		<label id="onPress"></label>
	</div>

	<script type="text/javascript">
		function edValueKeyPress() {
			document.getElementById('onPress').innerHTML = document.getElementById('txt_any').value
		}
	</script>

</body>

</html>