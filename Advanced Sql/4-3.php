<!DOCTYPE html>
<html>
<head>
	<title>4-3</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
	
	<div  style="padding: 30px">
		<h1>Using Case conditional statement</h1>
		</textarea> 

		<textarea class="form-control" type="text" name="" disabled rows="10">
SELECT first_name,middle_name,last_name,
CASE
    WHEN positions.name = 'CEO' THEN "Chief Executive Officer"
    WHEN positions.name = 'CTO' THEN "Chief Technical Officer"
    WHEN positions.name = 'CFO' THEN "Chief Financial Officer"
    ELSE positions.name 
END  position

FROM employees INNER JOIN employee_positions on employees.id = employee_id INNER JOIN positions on positions.id = position_id
		</textarea> 
		
	 </div>
	
</body>
</html>