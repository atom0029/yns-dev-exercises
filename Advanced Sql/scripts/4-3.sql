SELECT first_name,middle_name,last_name,
CASE
    WHEN positions.name = 'CEO' THEN "Chief Executive Officer"
    WHEN positions.name = 'CTO' THEN "Chief Technical Officer"
    WHEN positions.name = 'CFO' THEN "Chief Financial Officer"
    ELSE positions.name 
END  position

FROM employees INNER JOIN employee_positions on employees.id = employee_id INNER JOIN positions on positions.id = position_id