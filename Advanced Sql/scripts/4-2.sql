use yns;

CREATE TABLE IF NOT EXISTS therapists(
id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
name		 VARCHAR(100) NOT NULL,
);

CREATE TABLE IF NOT EXISTS daily_work_shifts(
id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
therapist_id int,
target_date date,
start_time time,
end_time time,
);

insert into therapists values('','John');
insert into therapists values('','Arnold');
insert into therapists values('','Robert');
insert into therapists values('','Ervin');
insert into therapists values('','Smith');

insert into daily_work_shifts values('',1,NOW(),'14:00:00','15:00:00');
insert into daily_work_shifts values('',2,NOW(),'22:00:00','23:00:00');
insert into daily_work_shifts values('',3,NOW(),'00:00:00','01:00:00');
insert into daily_work_shifts values('',4,NOW(),'5:00:00','5:30:00');
insert into daily_work_shifts values('',1,NOW(),'21:00:00','21:45:00');
insert into daily_work_shifts values('',5,NOW(),'5:30:00','5:50:00');
insert into daily_work_shifts values('',3,NOW(),'2:00:00','2:30:00');

SELECT therapist_id,target_date,start_time,end_time,
CASE
    WHEN start_time  <= ' 05:59:59' and start_time  >= ' 00:00:00'
    THEN  CONCAT(target_date + interval 1 day, ' ' , start_time)
    ELSE CONCAT(target_date , ' ' , start_time) 
END sort_start_time
from daily_work_shifts
inner join therapists on therapists.id = therapist_id
order by target_date asc, sort_start_time ASC;