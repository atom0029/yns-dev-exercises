<?php
    if (isset($_POST['submit'])) {
        $isFirstNameValid = testAlpha($_POST["firstName"]);
        $isMiddleNameValid = testAlpha($_POST["middleName"]);
        $isLastNameValid = testAlpha($_POST["lastName"]);
    }

    function testAlpha($data)
    {
        $blacklistChars = '"%\'*;<>?^`{|}~/\\#=&';

        $pattern = preg_quote($blacklistChars, '/');
        if (preg_match('/[0-9]+/', $data) || preg_match('/[' . $pattern . ']/', $data)) {
            return false;
        } else {
            return true;
        }
    }


    
?>


<!DOCTYPE html>
<html>

<head>
	<title>1-7</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
	<?php
        if (isset($isFirstNameValid) && !$isFirstNameValid) {
            echo "<p>No number and special char allowed in firstname</p>";
        }
        if (isset($isMiddleNameValid) && !$isMiddleNameValid) {
            echo "<p>No number and special char allowed in middlename </p>";
        }
        if (isset($isLastNameValid) && !$isLastNameValid) {
            echo "<p>No number and special char allowed in lastname </p>";
        }
        
    ?>
	<form method="POST" style="padding: 40px">
		<div class="form-group">
			<label for="firstName">First Name: *</label>
			<input type="text" class="form-control" name="firstName" required>
		</div>
		<div class="form-group">
			<label for="middleName">Middle Name:</label>
			<input type="text" class="form-control" name="middleName">
		</div>
		<div class="form-group">
			<label for="lastName">Last Name: *</label>
			<input type="text" class="form-control" name="lastName" required>
		</div>
		<div class="form-group">
			<label for="email">Email: *</label>
			<input type="email" class="form-control" name="email" required>
		</div>
		<div class="form-group">
			<label for="digit">Digit:</label>
			<input type="number" class="form-control" name="digit">
		</div>
		<div class="form-group">
			<label for="txt_date">Birth Date: *</label>
			<input type="date" class="form-control" name="date"
				max="<?php echo date('Y-m-d'); ?>"
				required>
		</div>


		<button type="submit" class="btn btn-default" name="submit">Send</button>
	</form>
</body>

</html>