<!DOCTYPE html>
<html>

<head>
	<title>1-6</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
	<h3>1-6 Input</h3>
	<form method="POST" action="1-6-output.php" style="padding: 40px">
		<div class="form-group">
			<label for="txt_num1">First Name:</label>
			<input type="text" class="form-control" name="firstName">
		</div>
		<div class="form-group">
			<label for="txt_num1">Middle Name:</label>
			<input type="text" class="form-control" name="middleName">
		</div>
		<div class="form-group">
			<label for="txt_num1">Last Name:</label>
			<input type="text" class="form-control" name="lastName">
		</div>
		<div class="form-group">
			<label for="txt_num1">Email:</label>
			<input type="email" class="form-control" name="email">
		</div>
		<div class="form-group">
			<label for="txt_num1">Birth Date:</label>
			<input type="date" class="form-control" name="date">
		</div>
		<button type="submit" class="btn btn-default" name="submit">Send</button>
	</form>
</body>

</html>