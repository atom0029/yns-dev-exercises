<!DOCTYPE html>
<html>

<head>
	<title>1-6</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
	<h3>1-6 Output</h3>
	<form method="POST" style="padding: 40px">
		<div class="form-group">
			<label for="firstName">First Name:</label>
			<input type="text" class="form-control" name="firstName" value="<?php if (isset($_POST['firstName'])) {
    echo $_POST['firstName'];
} ?>" readonly>
		</div>
		<div class="form-group">
			<label for="middleName">Middle Name:</label>
			<input type="text" class="form-control" name="middleName" value="<?php if (isset($_POST['middleName'])) {
    echo $_POST['middleName'];
} ?>" readonly>
		</div>
		<div class="form-group">
			<label for="lastName">Last Name:</label>
			<input type="text" class="form-control" name="lastName" value="<?php if (isset($_POST['lastName'])) {
    echo $_POST['lastName'];
} ?>" readonly>
		</div>
		<div class="form-group">
			<label for="email">Email:</label>
			<input type="email" class="form-control" name="email" value="<?php if (isset($_POST['email'])) {
    echo $_POST['email'];
} ?>" readonly>
		</div>
		<div class="form-group">
			<label for="num1">Birth Date:</label>
			<input type="date" class="form-control" name="date" value="<?php if (isset($_POST['date'])) {
    echo $_POST['date'];
} ?>" readonly>
		</div>

	</form>
</body>

</html>