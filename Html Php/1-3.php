<?php
    if (isset($_POST['gcd'])) {
        $gcd = 0;
        $temp = 0;
        $num1 = $_POST['num1'];
        $num2 = $_POST['num2'];
        $num1 > $num2 ? $temp = $num1 : $temp = $num2;
        for ($i = 1; $i < $temp; $i++) {
            if ($num1 % $i == 0 && $num2 % $i == 0) {
                $gcd = $i;
            }
        }
    }
?>


<!DOCTYPE html>
<html>

<head>
	<title>1-3</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
	<h3> 
		<?php if(isset($gcd)) {
    		echo "GCD: " . $gcd;
		} ?>
	</h3>
	<form method="POST">
		<div class="form-group">
			<label for="num1">Number 1:</label>
			<input type="text" class="form-control" name="num1">
		</div>
		<div class="form-group">
			<label for="num2">Number 2:</label>
			<input type="text" class="form-control" name="num2">
		</div>

		<button type="submit" class="btn btn-default" name="gcd">Find Gcd</button>
	</form>
</body>

</html>