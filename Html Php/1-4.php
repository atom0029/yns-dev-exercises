<?php
    if (isset($_POST['fizzBuzz'])) {
        $fizzBuzz = [];
        $num1 = $_POST['num1'];
        $count = 0;
        for ($i = 1; $i <= $num1; $i++) {
            if ($i % 3 == 0 && $i % 5 == 0) {
                $fizzBuzz[$count] = 'fizzBuzz';
                $count++;
            } elseif ($i % 3 == 0) {
                $fizzBuzz[$count] = 'Fizz';
                $count++;
            } elseif ($i % 5 == 0) {
                $fizzBuzz[$count] = 'Buzz';
                $count++;
            }
        }
    }
?>


<!DOCTYPE html>
<html>

<head>
	<title>1-4</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>

	<form method="POST" style="padding: 30px">
		<div class="form-group">
			<label for="num1">Number:</label>
			<input type="text" class="form-control" name="num1">
		</div>


		<button type="submit" class="btn btn-default" name="fizzBuzz">Submit</button>
	</form>
	<h3 style="padding-left: 30px"> <?php
        if (isset($fizzBuzz)) {
            for ($j = 0; $j < $count; $j++) {
                echo $fizzBuzz[$j] . '<br>';
            }
        }
    ?>
	</h3>
</body>

</html>