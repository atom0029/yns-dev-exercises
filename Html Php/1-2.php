<?php 
	switch ($_SERVER["REQUEST_METHOD"] == "POST") {
		case isset($_POST['addition']):
			$operand = 'Addition';
			$result = $_POST['txt_num1'] + $_POST['txt_num2'];
			
			break;
		case isset($_POST['subtract']):
			$operand = 'Subtraction';
			$result = $_POST['txt_num1'] - $_POST['txt_num2'];
			
			break;
		case isset($_POST['multiply']):
			$operand = 'Multiplication';
			$result = $_POST['txt_num1'] * $_POST['txt_num2'];
			
			break;
		case isset($_POST['division']):
			$operand = 'Division';
			$result = $_POST['txt_num1'] / $_POST['txt_num2'];
			break;
		default:
			break;
	}
	
?>

<!DOCTYPE html>
<html>
<head>
	<title>1-2</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

</head>
<body>
	<h3>Operation: <?php if(isset($operand)) echo $operand; ?></h3>
	<h3>Result: <?php if(isset($result)) echo $result; ?></h3>
	<form method="POST" >
		<div class="form-group">
			<label for="txt_num1">Number 1:</label>
			<input type="text" class="form-control" name="txt_num1">
		</div>
		<div class="form-group">
			<label for="txt_num2">Number 2:</label>
			<input type="text" class="form-control" name="txt_num2">
		</div>
	  
	  	<button type="submit" class="btn btn-default" name="addition" >Add</button>
	  	<button type="submit" class="btn btn-default" name="subtract">Subract</button>
	  	<button type="submit" class="btn btn-default" name="multiply">Multiply</button>
		<button type="submit" class="btn btn-default" name="division">Divide</button>
	</form>
</body>
</html>