<?php
    if (isset($_POST['submit'])) {
        $isFirstNameValid = testAlpha($_POST['firstName']);
        $isMiddleNameValid = testAlpha($_POST['middleName']);
        $isLastNameValid = testAlpha($_POST['lastName']);

        if ($isFirstNameValid && $isMiddleNameValid && $isLastNameValid) {
			$file = fopen('contact_data.csv', 'a');
			$rowCount = count(file('contact_data.csv'));
			if ($rowCount > 1) {
			    $rowCount = ($rowCount - 1) + 1;
			}
			$formData = array(
			   'firstname'  => $_POST['firstName'],
			   'middlename'  => $_POST['middleName'],
			   'lastname'  => $_POST['lastName'],
			   'email' => $_POST['email'],
			   'password' => $_POST['password'],
			   'number' => $_POST['digit'],
			   'txt_date' => $_POST['date']
			);
			fputcsv($file, $formData);
        }
    }

    function testAlpha($data)
    {
        $blacklistChars = '"%\'*;<>?^`{|}~/\\#=&';

        $pattern = preg_quote($blacklistChars, '/');
        if (preg_match('/[0-9]+/', $data) || preg_match('/[' . $pattern . ']/', $data)) {
            return false;
        } 
        else {
            return true;
        }
    }
?>


<!DOCTYPE html>
<html>

<head>
	<title>1-9</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
	<?php
        if (isset($firstname_valid) && !$firstname_valid) {
            echo "<p>No number and special char allowed in firstname</p>";
        }
        if (isset($middlename_valid) && !$middlename_valid) {
            echo "<p>No number and special char allowed in middlename </p>";
        }
        if (isset($lastname_valid) && !$lastname_valid) {
            echo "<p>No number and special char allowed in lastname </p>";
        }
        
    ?>
	<form method="POST" style="padding: 40px">
		<div class="form-group">
			<label for="firstName">First Name: *</label>
			<input type="text" class="form-control" name="firstName" required>
		</div>
		<div class="form-group">
			<label for="middleName">Middle Name:</label>
			<input type="text" class="form-control" name="middleName">
		</div>
		<div class="form-group">
			<label for="lastName">Last Name: *</label>
			<input type="text" class="form-control" name="lastName" required>
		</div>
		<div class="form-group">
			<label for="email">Email: *</label>
			<input type="email" class="form-control" name="email" required>
		</div>
		<div class="form-group">
			<label for="password">Password: *</label>
			<input type="password" class="form-control" name="password" required>
		</div>
		<div class="form-group">
			<label for="digit">Digit:</label>
			<input type="number" class="form-control" name="digit">
		</div>
		<div class="form-group">
			<label for="date">Birth Date: *</label>
			<input type="date" class="form-control" name="date"
				max="<?php echo date('Y-m-d'); ?>"
				required>
		</div>


		<button type="submit" class="btn btn-default" name="submit">Submit</button>
	</form>

	<table class="table" style="margin: 40px">
		<thead>
			<th>First Name</th>
			<th>Middle Name</th>
			<th>Last Name</th>
			<th>Email</th>
			<th>Password</th>
			<th>Number</th>
			<th>Birth Date</th>
			<th>Profile</th>
		</thead>
		<tbody>
			<?php  $file = fopen("contact_data.csv", "r");  ?>
			<?php while (($line = fgetcsv($file)) !== false): ?>
				<tr>
					<?php foreach ($line as $headercolumn): ?>
						<th><?= $headercolumn; ?></th>
					<?php  endforeach; ?>

				</tr>
			<?php  endwhile; ?>
		</tbody>
	</table>
</body>

</html>