<?php
    session_start();
    if (isset($_SESSION['user'])) {
        // logged in
    } else {
        header('Location: 1-13.php');
    }

    if (isset($_POST['submit'])) {
        $firstname_valid = testAlpha($_POST['firstName']);
        $middlename_valid = testAlpha($_POST['middleName']);
        $lastname_valid = testAlpha($_POST['lastName']);
        if (!is_dir('upload')) {
            mkdir('upload');
        }

        if ($_FILES['fileToUpload']['size'] == 0) {
            $image_valid = false;
        } 

        else {
            $filename = $_FILES['fileToUpload']['name'];

            $tempname = $_FILES['fileToUpload']['tmp_name'];

            $folder = 'upload/'.$filename;
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($folder, PATHINFO_EXTENSION));

            // Check if image file is a actual image or fake image
            if (isset($_POST['submit'])) {
                $check = getimagesize($_FILES['fileToUpload']['tmp_name']);
                if ($check !== false) {
                    echo "File is an image - " . $check["mime"] . ".";
                    $uploadOk = 1;
                } else {
                    echo 'File is not an image.';
                    $uploadOk = 0;
                }
            }

            // Check if file already exists
            if (file_exists($folder)) {
            	echo 'Sorry, file already exists.';
            	$uploadOk = 0;
            }

            // Check file size
            if ($_FILES['fileToUpload']['size'] > 500000) {
                echo 'Sorry, your file is too large.';
                $uploadOk = 0;
            }

            // Allow certain file formats
            if ($imageFileType != 'jpg' && $imageFileType != 'png' && $imageFileType != 'jpeg' && $imageFileType != 'gif') {
                echo 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.';
                $uploadOk = 0;
            }

            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo 'Sorry, your file was not uploaded.';

            // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($tempname, $folder)) {
                    // echo "The file ". htmlspecialchars( basename( $_FILES["fileToUpload"]["name"])). " has been uploaded.";
                    if ($firstname_valid && $middlename_valid && $lastname_valid) {
                        $file = fopen("contact_data.csv", "a");
                        $countRows = count(file("contact_data.csv"));
                        if ($countRows > 1) {
                            $countRows = ($countRows - 1) + 1;
                        }
                        $formData = array(
                            'firstName'  => $_POST["firstName"],
                            'middleName'  => $_POST["middleName"],
                            'lastName'  => $_POST["lastName"],
                            'email' => $_POST["email"],
                            'password' => $_POST["password"],
                            'number' => $_POST["digit"],
                            'date' => $_POST["date"],
                            'profile' => $_FILES["fileToUpload"]["name"]
                        );
                        fputcsv($file, $formData);
                    }
                } 
                else {
                    echo 'Sorry, there was an error uploading your file.';
                }
            }
        }
    }

    function testAlpha($data)
    {
        $blacklistChars = '"%\'*;<>?^`{|}~/\\#=&';
        $pattern = preg_quote($blacklistChars, '/');
        if (preg_match('/[0-9]+/', $data) || preg_match('/[' . $pattern . ']/', $data)) {
            return false;
        }
        else {
            return true;
        }
    }

    $arrayPage = [];
    $csvToArray = [];
    $file = fopen('contact_data.csv', 'r');
    while (($line = fgetcsv($file)) !== false) {
        array_push($csvToArray, $line);
    }
    fclose($file);
    $currentPage = $_GET['page'] ?? 1;
    $total = count($csvToArray);
    $page = ceil($total / 10);
    $arrayPage = array_slice($csvToArray, ($currentPage - 1) * 10, 10);

    if (isset($_POST['logout'])) {
        session_destroy();
        header('Location: 1-13.php');
    }

?>


<!DOCTYPE html>
<html>

<head>
	<title>1-12</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
	<form method="post">
		<button type="submit" class="btn btn-danger" name="logout" style="margin: 20px">Logout</button>
	</form>
	<?php
    if (isset($firstname_valid) && !$firstname_valid) {
        echo "<p>No number and special char allowed in firstname</p>";
    }
    if (isset($middlename_valid) && !$middlename_valid) {
        echo "<p>No number and special char allowed in middlename </p>";
    }
    if (isset($lastname_valid) && !$lastname_valid) {
        echo "<p>No number and special char allowed in lastname </p>";
    }
    if (isset($image_valid) && !$image_valid) {
        echo "<p>Image is required </p>";
    }
    ?>

	<form method="POST" style="padding: 40px" enctype="multipart/form-data">
		<div class="form-group">
			<label for="firstName">First Name: *</label>
			<input type="text" class="form-control" name="firstName" required>
		</div>
		<div class="form-group">
			<label for="middleName">Middle Name:</label>
			<input type="text" class="form-control" name="middleName">
		</div>
		<div class="form-group">
			<label for="lastName">Last Name: *</label>
			<input type="text" class="form-control" name="lastName" required>
		</div>
		<div class="form-group">
			<label for="email">Email: *</label>
			<input type="email" class="form-control" name="email" required>
		</div>
		<div class="form-group">
			<label for="lastname">Password: *</label>
			<input type="password" class="form-control" name="password" required>
		</div>

		<div class="form-group">
			<label for="digit">Digit:</label>
			<input type="number" class="form-control" name="digit">
		</div>
		<div class="form-group">
			<label for="date">Birth Date: *</label>
			<input type="date" class="form-control" name="date"
				max="<?php echo date('Y-m-d'); ?>"
				required>
		</div>
		<div class="form-group">
			<label for="fileToUpload">Select image to upload: *</label>
			<input type="file" name="fileToUpload" id="fileToUpload">
		</div>


		<button type="submit" class="btn btn-default" name="submit">Submit</button>
	</form>

	<table class="table" style="margin: 40px">
		<thead>
			<th>First Name</th>
			<th>Middle Name</th>
			<th>Last Name</th>
			<th>Email</th>
			<th>Number</th>
			<th>Birth Date</th>
			<th>Profile</th>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php  if (isset($arrayPage)):  ?>
			<?php foreach ($arrayPage as $key => $value): ?>
			<tr>
				<th><?= $value[0]; ?>
				</th>
				<th><?= $value[1]; ?>
				</th>
				<th><?= $value[2]; ?>
				</th>
				<th><?= $value[3]; ?>
				</th>
				<th><?= $value[5]; ?>
				</th>
				<th><?= $value[6]; ?>
				</th>
				<th><?php if (isset($value[7])) {
                        echo '<img style="height:50px" src='.'upload/'. $value[7]. '>';
                    } ?>
				</th>
			</tr>
			<?php endforeach;  endif; ?>

		</tbody>
		<tfoot style="margin-left: 40px">
			Page:
			<?php for ($j=1 ; $j<=$page; $j++) {
                echo '<a href="1-12.php?page='.$j.'"> '.$j.'</a>';
            } ?>
		</tfoot>
	</table>
</body>
</html>