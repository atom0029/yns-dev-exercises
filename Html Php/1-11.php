<?php
    if (isset($_POST['submit'])) {
        $isFirstNameValid = testAlpha($_POST['firstName']);
        $isMiddleNameValid = testAlpha($_POST['middleName']);
        $isLastNameValid = testAlpha($_POST['lastName']);
        if (!is_dir('upload')) {
            mkdir('upload');
        }
        if ($_FILES['fileToUpload']['size'] == 0) {
            $image_valid = false;
        } else {
            $fileName = $_FILES['fileToUpload']['name'];

            $tempname = $_FILES['fileToUpload']['tmp_name'];

            $folder = 'upload/'.$fileName;
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($folder, PATHINFO_EXTENSION));

            // Check if image file is a actual image or fake image
            if (isset($_POST['submit'])) {
                $check = getimagesize($_FILES['fileToUpload']['tmp_name']);
                if ($check !== false) {
                    echo "File is an image - " . $check["mime"] . ".";
                    $uploadOk = 1;
                } 
                else {
                    echo 'File is not an image.';
                    $uploadOk = 0;
                }
            }

            // Check if file already exists
            if (file_exists($folder)) {
                echo 'Sorry, file already exists.';
                $uploadOk = 0;
            }

            // Check file size
            if ($_FILES['fileToUpload']['size'] > 500000) {
                echo 'Sorry, your file is too large.';
                $uploadOk = 0;
            }

            // Allow certain file formats
            if ($imageFileType != 'jpg' && $imageFileType != 'png' && $imageFileType != 'jpeg' && $imageFileType != 'gif') {
                echo 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.';
                $uploadOk = 0;
            }

            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo 'Sorry, your file was not uploaded.';
            // if everything is ok, try to upload file
            } 
            else {
                if (move_uploaded_file($tempname, $folder)) {
                    if ($isFirstNameValid && $isMiddleNameValid && $isLastNameValid) {
                        $file = fopen('contact_data.csv', 'a');
                        $rowCount = count(file('contact_data.csv'));
                        if ($rowCount > 1) {
                            $rowCount = ($rowCount - 1) + 1;
                        }
                        $formData = array(
                           'firstname'  => $_POST['firstName'],
                           'middlename'  => $_POST['middleName'],
                           'lastname'  => $_POST['lastName'],
                           'email' => $_POST['email'],
                           'password' => $_POST['password'],
                           'number' => $_POST['digit'],
                           'txt_date' => $_POST['date'],
                           'profile' => $_FILES['fileToUpload']['name']
                        );
                        fputcsv($file, $formData);
                    }
                } 
                else {
                    echo 'Sorry, there was an error uploading your file.';
                }
            }
        }
    }

    function testAlpha($data)
    {
        $blacklistChars = '"%\'*;<>?^`{|}~/\\#=&';
        $pattern = preg_quote($blacklistChars, '/');
        if (preg_match('/[0-9]+/', $data) || preg_match('/[' . $pattern . ']/', $data)) {
            return false;
        } 
        else {
            return true;
        }
    }
?>


<!DOCTYPE html>
<html>

<head>
	<title>1-10</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
	<?php
    if (isset($isFirstNameValid) && !$isFirstNameValid) {
        echo "<p>No number and special char allowed in firstname</p>";
    }
    if (isset($isMiddleNameValid) && !$isMiddleNameValid) {
        echo "<p>No number and special char allowed in middlename </p>";
    }
    if (isset($isLastNameValid) && !$isLastNameValid) {
        echo "<p>No number and special char allowed in lastname </p>";
    }
    if (isset($image_valid) && !$image_valid) {
        echo "<p>Image is required </p>";
    }
    ?>
	<form method="POST" style="padding: 40px" enctype="multipart/form-data">
		<div class="form-group">
			<label for="firstName">First Name: *</label>
			<input type="text" class="form-control" name="firstName" required>
		</div>
		<div class="form-group">
			<label for="middleName">Middle Name:</label>
			<input type="text" class="form-control" name="middleName">
		</div>
		<div class="form-group">
			<label for="lastName">Last Name: *</label>
			<input type="text" class="form-control" name="lastName" required>
		</div>
		<div class="form-group">
			<label for="email">Email: *</label>
			<input type="email" class="form-control" name="email" required>
		</div>
		<div class="form-group">
			<label for="password">Password: *</label>
			<input type="password" class="form-control" name="password" required>
		</div>
		<div class="form-group">
			<label for="digit">Digit:</label>
			<input type="number" class="form-control" name="digit">
		</div>
		<div class="form-group">
			<label for="txt_date">Birth Date: *</label>
			<input type="date" class="form-control" name="date"
				max="<?php echo date('Y-m-d'); ?>"
				required>
		</div>
		<div class="form-group">
			<label for="txt_date">Select image to upload: *</label>
			<input type="file" name="fileToUpload" id="fileToUpload">
		</div>
		<button type="submit" class="btn btn-default" name="submit">Submit</button>
	</form>
	<table class="table" style="margin: 40px">
		<thead>
			<th>First Name</th>
			<th>Middle Name</th>
			<th>Last Name</th>
			<th>Email</th>
			<th>Password</th>
			<th>Number</th>
			<th>Birth Date</th>
			<th>Profile</th>
		</thead>
		<tbody>
			<?php $file = fopen('contact_data.csv', 'r'); ?>
			<?php while (($line = fgetcsv($file)) !== false): ?>
    			<tr>
    				<?php for ($i = 0; $i<count($line); $i++): ?>
    				    <th> <?= $line[$i]; ?> </th>
    				<?php endfor; ?>
    			</tr>
			<?php endwhile; ?>
		</tbody>
	</table>
</body>
</html>