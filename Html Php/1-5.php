<?php
    if (isset($_POST['submit'])) {
        $origDate = $_POST['date'];
        $dates = [];
        $datesNames = [];
        $dates[0] = $origDate;
        $datesNames[0] = date('D', strtotime($dates[0]));
        // echo $dates[0];
        for ($i = 1 ; $i <= 3; $i++) {
            $day = $i . 'day';
            $dates[$i] = date('Y-m-d', strtotime($day, strtotime($origDate)));
            $datesNames[$i] = date('D', strtotime($dates[$i]));
        }
    }
?>


<!DOCTYPE html>
<html>

<head>
	<title>1-5</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
	<h3> <?php
        if (isset($dates) && isset($datesNames)) {
            for ($j = 0; $j < count($dates); $j++) {
                echo $dates[$j] . '  ' . $datesNames[$j] . '<br>';
            }
        }
    ?>
	</h3>
	<form method="POST">
		<div class="form-group">
			<label for="txt_num1">Date:</label>
			<input type="date" class="form-control" name="date">
		</div>


		<button type="submit" class="btn btn-default" name="submit">Submit</button>
	</form>
</body>

</html>