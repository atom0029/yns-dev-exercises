<?php
 session_start();
 if (isset($_SESSION['user'])) {
     header('Location: 1-12.php');
 }


    if (isset($_POST['btn_submit'])) {
        $user = $_POST['username'];
        $pass = $_POST['password'];
        $f = fopen('contact_data.csv', 'r');
        while (($line = fgetcsv($f)) !== false) {
            if ($user == $line[3] && $pass == $line[4]) {
                $_SESSION['user'] = $user;
                header('Location: 1-12.php');
            }
        }
        fclose($f);
        $error = 'Invalid User or password';
    }
?>


<!DOCTYPE html>
<html>

<head>
	<title>1-13</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
	<h3> 
		<?php if (isset($error)) {
    		echo $error;
		} ?>
	</h3>
	<form method="POST" style="padding: 30px">
		<div class="form-group">
			<label for="username">Email:</label>
			<input type="text" class="form-control" name="username">
		</div>
		<div class="form-group">
			<label for="password">Password:</label>
			<input type="password" class="form-control" name="password">
		</div>

		<button type="submit" class="btn btn-default" name="btn_submit">Login</button>
	</form>
</body>

</html>