<!DOCTYPE html>
<html>
<head>
	<title>3-3</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
	
	<div  style="padding: 30px">
		<h1>Preparing database</h1>
		</textarea> 

		<textarea class="form-control" type="text" name="" disabled rows="10">
DROP DATABASE IF EXISTS yns;
CREATE DATABASE yns;
USE yns;


CREATE TABLE IF NOT EXISTS departments(
id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
name VARCHAR(50) NOT NULL
);


CREATE TABLE IF NOT EXISTS positions(
id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
name VARCHAR(50) NOT NULL
);


CREATE TABLE IF NOT EXISTS employees(
id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
first_name		 VARCHAR(50) NOT NULL,
last_name		 VARCHAR(50) NOT NULL,
middle_name		 VARCHAR(50) NULL,
birth_date		 DATE NOT NULL,
department_id		 INT NOT NULL,
hire_date		 DATE  NULL,
boss_id		 INT  NULL,
CONSTRAINT emp_fk_dept_id FOREIGN KEY (department_id) REFERENCES departments (id)
);


ALTER TABLE employees
ADD CONSTRAINT emp_fk_boss_id FOREIGN KEY (boss_id) REFERENCES employees (id);


CREATE TABLE IF NOT EXISTS employee_positions(
id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
employee_id		 INT NOT NULL,
position_id		 INT  NOT NULL,
CONSTRAINT emp_fk_emp_id FOREIGN KEY (employee_id) REFERENCES employees (id),
CONSTRAINT emp_fk_pos_id  FOREIGN KEY (position_id) REFERENCES positions (id)
);


INSERT INTO departments VALUES('','Exective');
INSERT INTO departments VALUES('','Admin');
INSERT INTO departments VALUES('','Sales');
INSERT INTO departments VALUES('','Development');
INSERT INTO departments VALUES('','Design');
INSERT INTO departments VALUES('','Marketing');


INSERT INTO positions VALUES('','CEO');
INSERT INTO positions VALUES('','CTO');
INSERT INTO positions VALUES('','CFO');
INSERT INTO positions VALUES('','Manager');
INSERT INTO positions VALUES('','Staff');


INSERT INTO employees VALUES('','Manabu','Yamazaki',null,'1976-03-15','1',null,null);
INSERT INTO employees VALUES('','Tomohiko','Takasago',null,'1974-05-24','3','2014-04-01','1');
INSERT INTO employees VALUES('','Yuta','Kawakami',null,'1990-08-13','4','2014-04-01','1');
INSERT INTO employees VALUES('','Shogo','Kubota',null,'1985-01-31','4','2014-12-01','1');
INSERT INTO employees VALUES('','Lorraine','San Jose','P.','1983-10-11','2','2015-03-10','1');
INSERT INTO employees VALUES('','Haille','Dela Cruz','A.','1990-11-12','3','2015-02-15','2');
INSERT INTO employees VALUES('','Godfrey','Sarmenta','L.','1993-09-13','4','2015-01-01','1');
INSERT INTO employees VALUES('','Alex','Amistad','F.','1988-04-14','4','2015-04-10','1');
INSERT INTO employees VALUES('','Hideshi','Ogoshi',null,'1983-07-15','4','2014-06-01','1');
INSERT INTO employees VALUES('','Kim',' ',' ','1977-10-16','5','2015-08-06','1');


INSERT INTO employee_positions VALUES('','1','1');
INSERT INTO employee_positions VALUES('','1','2');
INSERT INTO employee_positions VALUES('','1','3');
INSERT INTO employee_positions VALUES('','2','4');
INSERT INTO employee_positions VALUES('','3','5');
INSERT INTO employee_positions VALUES('','4','5');
INSERT INTO employee_positions VALUES('','5','5');
INSERT INTO employee_positions VALUES('','6','5');
INSERT INTO employee_positions VALUES('','7','5');
INSERT INTO employee_positions VALUES('','8','5');
INSERT INTO employee_positions VALUES('','9','5');
INSERT INTO employee_positions VALUES('','10','5');				
		</textarea> 

		<h1>SQL problems scripts</h1>
		</textarea> 

		<textarea class="form-control" type="text" name="" disabled rows="10">
use yns;

-- #1
select * from employees WHERE last_name LIKE 'k%';

-- #2
select * from employees WHERE last_name LIKE '%i';

-- #3
select concat(first_name,' ',middle_name,' ',last_name) full_name,hire_date from employees WHERE hire_date BETWEEN '2015/1/1' AND '2015/3/31' ORDER BY hire_date ASC;

-- #4
select e.last_name Employee,b.last_name Boss from employees e
LEFT JOIN employees b on e.boss_id = b.id
WHERE e.boss_id IS NOT NULL;

-- #5
SELECT last_name FROM employees
left JOIN departments ON department_id = departments.id
WHERE departments.name = "Sales"
ORDER BY last_name desc;

-- #6
SELECT COUNT(*) count_has_middle FROM employees WHERE middle_name is not null AND middle_name != '';

-- #7
SELECT departments.name,count(employees.id) from departments 
JOIN employees on departments.id = department_id
GROUP BY department_id;

-- #8
SELECT first_name,middle_name,last_name,hire_date from employees ORDER BY hire_date DESC LIMIT 1;

-- #9
SELECT name,id from departments d WHERE not  EXISTS (SELECT department_id from employees e WHERE d.id = department_id);

-- #10
SELECT first_name,middle_name,last_name from employees e WHERE e.id in (SELECT employee_id 
FROM employee_positions 
GROUP BY employee_id 
HAVING COUNT(*) > 2);
			
		</textarea> 
		
	 </div>
	
</body>
</html>