<!DOCTYPE html>
<html>
<head>
	<title>3-2</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
	
	<div  style="padding: 30px">
		<h1>Create a table script</h1>
		</textarea> 

		<textarea class="form-control" type="text" name="" disabled rows="10">
DROP DATABASE IF EXISTS yns;
CREATE DATABASE yns;
USE yns;
CREATE TABLE IF NOT EXISTS users(
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
firstname VARCHAR(30) NOT NULL,
lastname VARCHAR(30) NOT NULL,
email VARCHAR(50),
reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);
		</textarea> 
		
	 </div>
	
</body>
</html>