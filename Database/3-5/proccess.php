<?php
    // session_start();

    $con = new mysqli('mysql-server', 'root', 'secret', 'yns') or die(mysqli_error($con));
    if (session_status() === PHP_SESSION_NONE) {
        session_start();
    }

    if (isset($_SESSION['user'])) {
        $query = "Select employees.id boss_id,concat(first_name,' ' ,last_name) boss_name from employees join employee_positions on employee_id = employees.id join positions on position_id = positions.id where positions.id < 5 GROUP BY employees.id";
        $boss = $con->query($query);
        $query = "Select * from positions";
        $positions = $con->query($query);

        $query = "Select * from departments";
        $departments = $con->query($query);

        $query = "Select employees.id,first_name,middle_name,last_name,email,departments.name department,positions.name position,profile_image from employees join departments on department_id = departments.id JOIN employee_positions on employee_id = employees.id join positions on position_id = positions.id";
        $emp = $con->query($query);
        $array_page = [];
        $emp_to_array = [];
        while (($row = $emp->fetch_assoc()) !== null) {
            array_push($emp_to_array, $row);
        }
        
        $current_page = $_GET['page'] ?? 1;
        $total = count($emp_to_array);
        $page = ceil($total / 10);
        $array_page = array_slice($emp_to_array, ($current_page - 1) * 10, 10);
    }


    if (isset($_POST['login'])) {
        $user = $_POST['username'];
        $pass = $_POST['password'];
        $stmt = $con->prepare("Select count(*)  from employees where email = ? and password = ?");
        $stmt->bind_param('ss', $user, $pass);
        $stmt->execute();
        $result = $stmt->get_result();
        $row = $result->fetch_assoc();
        if ($row['count(*)'] == 0) {
            $error = 'Invalid User or password';
        } else {
            $_SESSION['user'] = $user;
            header('Location: 3-5.php');
        }
    }


    if (isset($_POST['send'])) {
        $firstname_valid = testAlpha($_POST['firstname']);
        $middlename_valid = testAlpha($_POST['middlename']);
        $lastname_valid = testAlpha($_POST['lastname']);
        
        if ($firstname_valid && $middlename_valid && $lastname_valid) {
            checkFolder();
            $filename = $_FILES['fileToUpload']['name'];
            $tempname = $_FILES['fileToUpload']['tmp_name'];
            $folder = "upload/".$filename;
            $image = isImageValid($tempname, $folder);


            switch ($image) {
                case 0:
                    echo 'Sorry, your file was not uploaded.';
                    break;
                case 1:
                    if (move_uploaded_file($tempname, $folder)) {
                        $firstname  = $_POST["firstname"];
                        $lastname  = $_POST["lastname"];
                        $middlename  = $_POST["middlename"];
                        $birth_date = $_POST["txt_date_birth"];
                        $department = $_POST["department_id"];
                        $hire_date = $_POST["txt_date_hired"];
                        $boss = $_POST["boss_id"];
                        $password = $_POST["password"];
                        $profile = $_FILES["fileToUpload"]["name"];
                        $email = $_POST["email"];
                        
                        $position  = $_POST["postion_id"];

                        // Insert in Employee table
                        $stmt = $con->prepare("INSERT INTO employees(first_name,last_name,middle_name,birth_date,department_id,hire_date,boss_id,password,profile_image,email) 
							VALUES(?,?,?,?,?,?,?,?,?,?)");
                        $stmt->bind_param('sssdidisss', $firstname, $lastname, $middlename, $birth_date, $department, $hire_date, $boss, $password, $profile, $email);
                        $stmt->execute();
                        $emp = $stmt->insert_id;


                        // Insert in Employee_position table
                        $stmt = $con->prepare("INSERT INTO employee_positions(employee_id,position_id) VALUES(?,?)");
                        $stmt->bind_param('ii', $emp, $position);
                        $stmt->execute();
                    } 
                    else {
                        echo 'Something went wrong when we are trying to upload this file';
                    }
                    break;
                default:
                    echo 'Something went wrong';
                    break;
            }
        }
    }


    function isImageValid($tempname, $folder)
    {
        if ($_FILES['fileToUpload']['size'] == 0) {
            $image_valid = false;
        } else {
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($folder, PATHINFO_EXTENSION));

            // Check if image file is a actual image or fake image
            if (isset($_POST['submit'])) {
                $check = getimagesize($_FILES['fileToUpload']['tmp_name']);
                if ($check !== false) {
                    echo "File is an image - " . $check["mime"] . ".";
                    $uploadOk = 1;
                } else {
                    echo 'File is not an image.';
                    $uploadOk = 0;
                }
            }

            // Check if file already exists
            // if (file_exists($folder)) {
            // 	echo "Sorry, file already exists.";
            // 	$uploadOk = 0;
            // }

            // Check file size
            if ($_FILES['fileToUpload']['size'] > 500000) {
                echo 'Sorry, your file is too large.';
                $uploadOk = 0;
            }

            // Allow certain file formats
            if ($imageFileType != 'jpg' && $imageFileType != 'png' && $imageFileType != 'jpeg' && $imageFileType != 'gif') {
                echo 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.';
                $uploadOk = 0;
            }
        }

        return $uploadOk;
    }

    function testAlpha($data)
    {
        $blacklistChars = '"%\'*;<>?^`{|}~/\\#=&';

        $pattern = preg_quote($blacklistChars, '/');
        if (preg_match('/[0-9]+/', $data) || preg_match('/[' . $pattern . ']/', $data)) {
            return false;
        } else {
            return true;
        }
    }

    function checkFolder()
    {
        if (!is_dir("upload")) {
            mkdir("upload");
        }
    }
