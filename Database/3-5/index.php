<?php
     require_once 'proccess.php';
     if (isset($_SESSION['user'])) {
         header("Location: 3-5.php");
     }
?>
<!DOCTYPE html>
<html>

<head>
	<title>3-5</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
	<h3> <?php if (isset($error)) {
		    echo $error;
		} ?>
	</h3>
	<form method="POST" style="padding: 30px">
		<div class="form-group">
			<label for="username">Email:</label>
			<input type="text" class="form-control" name="username">
		</div>
		<div class="form-group">
			<label for="password">Password:</label>
			<input type="password" class="form-control" name="password">
		</div>

		<button type="submit" class="btn btn-default" name="login">Login</button>
	</form>
</body>

</html>