<?php
    require_once 'proccess.php';

    if (isset($_SESSION['user'])) {
        // logged in
    } else {
        header("Location: index.php");
    }

    if (isset($_POST['btn_logout'])) {
        session_destroy();
        header("Location: index.php");
    }

?>


<!DOCTYPE html>
<html>

<head>
	<title>3-5</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
	<form method="post">
		<button type="submit" class="btn btn-danger" name="btn_logout" style="margin: 20px">Logout</button>
	</form>
	<?php
    if (isset($firstname_valid) && !$firstname_valid) {
        echo "<p>No number and special char allowed in firstname</p>";
    }
    if (isset($middlename_valid) && !$middlename_valid) {
        echo "<p>No number and special char allowed in middlename </p>";
    }
    if (isset($lastname_valid) && !$lastname_valid) {
        echo "<p>No number and special char allowed in lastname </p>";
    }
    if (isset($image_valid) && !$image_valid) {
        echo "<p>Image is required </p>";
    }
    ?>
	<form method="POST" style="padding: 40px" enctype="multipart/form-data">
		<div class="form-group">
			<label for="firstname">First Name: *</label>
			<input type="text" class="form-control" name="firstname" required>
		</div>
		<div class="form-group">
			<label for="middlename">Middle Name:</label>
			<input type="text" class="form-control" name="middlename">
		</div>
		<div class="form-group">
			<label for="lastname">Last Name: *</label>
			<input type="text" class="form-control" name="lastname" required>
		</div>
		<div class="form-group">
			<label for="email">Email: *</label>
			<input type="email" class="form-control" name="email" required>
		</div>
		<div class="form-group">
			<label for="lastname">Password: *</label>
			<input type="password" class="form-control" name="password" required>
		</div>

		<div class="form-group">
			<label for="txt_date">Hired Date: *</label>
			<input type="date" class="form-control" name="txt_date_hired"
				max="<?php echo date('Y-m-d'); ?>"
				required>
		</div>
		<div class="form-group">
			<label for="txt_date">Birth Date: *</label>
			<input type="date" class="form-control" name="txt_date_birth"
				max="<?php echo date('Y-m-d'); ?>"
				required>
		</div>
		<div class="form-group">
			<label for="txt_date">Select image to upload: *</label>
			<input type="file" name="fileToUpload" id="fileToUpload">
		</div>
		<div class="form-group">
			<label for="txt_num1">Position:</label>
			<select class="form-control" name="postion_id">
				<?php foreach ($positions as $row): ?>
				<option
					value="<?php echo $row['id'] ?>">
					<?php echo $row['name']; ?>
				</option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="form-group">
			<label for="txt_num1">Department:</label>
			<select class="form-control" name="department_id">
				<?php foreach ($departments as $row): ?>
				<option
					value="<?php echo $row['id'] ?>">
					<?php echo $row['name']; ?>
				</option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="form-group">
			<label for="txt_num1">Boss Name:</label>
			<select class="form-control" name="boss_id">
				<option value="null">Not Applicable</option>
				<?php foreach ($boss as $row): ?>
				<option
					value="<?php echo $row['boss_id'] ?>">
					<?php echo $row['boss_name']; ?>
				</option>
				<?php endforeach; ?>
			</select>
		</div>



		<button type="submit" class="btn btn-default" name="send">Submit</button>
	</form>

	<table class="table" style="margin: 40px">
		<thead>
			<th>Profile</th>
			<th>First Name</th>
			<th>Middle Name</th>
			<th>Last Name</th>
			<th>Email</th>
			<th>Department</th>
			<th>Position</th>

		</thead>
		<div style="margin-right: 20px; position: absolute; right: 0">
			Page:
			<?php for ($j=1 ; $j<=$page; $j++) {
        echo '<a href="3-5-1.php?page='.$j.'"> '.$j.'</a>';
    } ?>
		</div>
		<tbody>
			<?php $i=0; ?>
			<?php  if (isset($array_page)):  ?>
			<?php foreach ($array_page as $key => $value): ?>
			<tr>








				<th><?php if (isset($value['profile_image'])) {
        echo '<img style="height:50px" src='.'upload/'. $value['profile_image']. '>';
    }  ?>
				</th>
				<th><?php echo $value['first_name']; ?>
				</th>
				<th><?php echo $value['middle_name']; ?>
				</th>
				<th><?php echo $value['last_name']; ?>
				</th>
				<th><?php echo $value['email']; ?>
				</th>
				<th><?php echo $value['department']; ?>
				</th>
				<th><?php echo $value['position']; ?>
				</th>



			</tr>
			<?php endforeach;  endif; ?>

		</tbody>
	</table>


</body>

</html>