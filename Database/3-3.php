<!DOCTYPE html>
<html>
<head>
	<title>3-3</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
	
	<div  style="padding: 30px">
		<h1>INSERT, UPDATE, DELETE script</h1>
		</textarea> 

		<textarea class="form-control" type="text" name="" disabled rows="10">
USE yns;

INSERT INTO users(firstname,lastname,email) values ('Anel Thom','','ANELTHOM.YNS@GMAIL.COM');

UPDATE users SET firstname = 'Thom' WHERE email = 'ANELTHOM.YNS@GMAIL.COM';

DELETE FROM users WHERE  email = 'ANELTHOM.YNS@GMAIL.COM';
		</textarea> 
		
	 </div>
	
</body>
</html>