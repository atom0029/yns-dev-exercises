DROP DATABASE IF EXISTS yns;
CREATE DATABASE yns;
USE yns;

CREATE TABLE IF NOT EXISTS departments(
id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
name VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS positions(
id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
name VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS employees(
id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
first_name		 VARCHAR(50) NOT NULL,
last_name		 VARCHAR(50) NOT NULL,
middle_name		 VARCHAR(50) NULL,
birth_date		 DATE NOT NULL,
department_id		 INT NOT NULL,
hire_date		 DATE  NULL,
boss_id		 INT  NULL,
CONSTRAINT emp_fk_dept_id FOREIGN KEY (department_id) REFERENCES departments (id)
);

ALTER TABLE employees
ADD CONSTRAINT emp_fk_boss_id FOREIGN KEY (boss_id) REFERENCES employees (id);

CREATE TABLE IF NOT EXISTS employee_positions(
id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
employee_id		 INT NOT NULL,
position_id		 INT  NOT NULL,
CONSTRAINT emp_fk_emp_id FOREIGN KEY (employee_id) REFERENCES employees (id),
CONSTRAINT emp_fk_pos_id  FOREIGN KEY (position_id) REFERENCES positions (id)
);


INSERT INTO departments VALUES('','Exective');
INSERT INTO departments VALUES('','Admin');
INSERT INTO departments VALUES('','Sales');
INSERT INTO departments VALUES('','Development');
INSERT INTO departments VALUES('','Design');
INSERT INTO departments VALUES('','Marketing');

INSERT INTO positions VALUES('','CEO');
INSERT INTO positions VALUES('','CTO');
INSERT INTO positions VALUES('','CFO');
INSERT INTO positions VALUES('','Manager');
INSERT INTO positions VALUES('','Staff');

INSERT INTO employees VALUES('','Manabu','Yamazaki',null,'1976-03-15','1',null,null);
INSERT INTO employees VALUES('','Tomohiko','Takasago',null,'1974-05-24','3','2014-04-01','1');
INSERT INTO employees VALUES('','Yuta','Kawakami',null,'1990-08-13','4','2014-04-01','1');
INSERT INTO employees VALUES('','Shogo','Kubota',null,'1985-01-31','4','2014-12-01','1');
INSERT INTO employees VALUES('','Lorraine','San Jose','P.','1983-10-11','2','2015-03-10','1');
INSERT INTO employees VALUES('','Haille','Dela Cruz','A.','1990-11-12','3','2015-02-15','2');
INSERT INTO employees VALUES('','Godfrey','Sarmenta','L.','1993-09-13','4','2015-01-01','1');
INSERT INTO employees VALUES('','Alex','Amistad','F.','1988-04-14','4','2015-04-10','1');
INSERT INTO employees VALUES('','Hideshi','Ogoshi',null,'1983-07-15','4','2014-06-01','1');
INSERT INTO employees VALUES('','Kim',' ',' ','1977-10-16','5','2015-08-06','1');



INSERT INTO employee_positions VALUES('','1','1');
INSERT INTO employee_positions VALUES('','1','2');
INSERT INTO employee_positions VALUES('','1','3');
INSERT INTO employee_positions VALUES('','2','4');
INSERT INTO employee_positions VALUES('','3','5');
INSERT INTO employee_positions VALUES('','4','5');
INSERT INTO employee_positions VALUES('','5','5');
INSERT INTO employee_positions VALUES('','6','5');
INSERT INTO employee_positions VALUES('','7','5');
INSERT INTO employee_positions VALUES('','8','5');
INSERT INTO employee_positions VALUES('','9','5');
INSERT INTO employee_positions VALUES('','10','5');



































 







							