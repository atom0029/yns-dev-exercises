use yns;

-- #1
select * from employees WHERE last_name LIKE 'k%';

-- #2
select * from employees WHERE last_name LIKE '%i';

-- #3
select concat(first_name,' ',middle_name,' ',last_name) full_name,hire_date from employees WHERE hire_date BETWEEN '2015/1/1' AND '2015/3/31' ORDER BY hire_date ASC;

-- #4
select e.last_name Employee,b.last_name Boss from employees e
LEFT JOIN employees b on e.boss_id = b.id
WHERE e.boss_id IS NOT NULL;

-- #5
SELECT last_name FROM employees
left JOIN departments ON department_id = departments.id
WHERE departments.name = "Sales"
ORDER BY last_name desc;

-- #6
SELECT COUNT(*) count_has_middle FROM employees WHERE middle_name is not null AND middle_name != '';

-- #7
SELECT departments.name,count(employees.id) from departments 
JOIN employees on departments.id = department_id
GROUP BY department_id;

-- #8
SELECT first_name,middle_name,last_name,hire_date from employees ORDER BY hire_date DESC LIMIT 1;

-- #9
SELECT name,id from departments d WHERE not  EXISTS (SELECT department_id from employees e WHERE d.id = department_id);

-- #10
SELECT first_name,middle_name,last_name from employees e WHERE e.id in (SELECT employee_id 
FROM employee_positions 
GROUP BY employee_id 
HAVING COUNT(*) > 2);
